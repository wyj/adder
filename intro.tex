\section{Introduction}
\label{introduction}

In the realm of 3GPP machine-type communications (MTC), protocols like NB-IoT and LTE-M employ energy-saving mechanisms such as DRX and PSM to extend battery life. Sensor-based applications, such as meters and measurement sensors,  re-establish network connections from "sleep" state immediately when there is Mobile Originated (MO) messages. The delay in the delivery of MO messages is generally attributed to the time needed for reconnection and  transmission latency. However, in the case of actuator-centric applications like Remote-Controlled Irrigation Systems, bike-sharing programs, Smart Traffic Control Systems, and Smart Lockers, the scenario is quite different. These actuator-centric applications primarily engage in infrequent downlink communication, where remote controllers send Mobile Terminated (MT) messages directing them to perform tasks such as irrigation, locking and unlocking, and controlling traffic signals. For devices in an inactive connection state, there might be a significant delay before they can react to the network's notification of incoming downlink traffic (i.e., a paging request), especially when these downlink communications appear suddenly and require immediate attention.

% this hinges on the server's ability to anticipate potential delays and adjust its confirmation timer accordingly. 
While certain actuator-centric applications, such as initiating irrigation on a farm, can tolerate delays in command execution, in other scenarios, prompt responses are essential; making a user wait several minutes to unlock a bike is impractical, and immediate traffic sign changes can avert accidents or reduce traffic congestion during peak hours. This delicate balance between energy consumption and response time varies not only across different applications but also within the same application.
Thus, the application developers face the challenge of not only understanding complex cellular network energy-saving mechanisms but also crafting a dynamic strategy that adapts to the application's unique traffic patterns. This motivate us to investigate the feasibility of learning an actuator application service's dynamics to dynamically adjust energy-saving mechanisms, aiming to strike the unique energy and latency trade-offs demanded by the service.

To achieve optimal energy savings while meeting latency constraints for actuator-centric applications, we propose Adder, a novel network-side solution that goes beyond device-specifc approaches. Adder leverages a context-aware traffic predictor that combines service provider insights with additional data like weather, traffic, and holidays to accurately forecast traffic patterns. These predictions then guide the modified DDPG-based~\cite{lillicrap2015continuous} policy maker in selecting the most energy-efficient DRX and PSM parameters. The network controller acknowledges these settings to devices, allowing them to adapt their communication accordingly. This Adder ensures optimal energy usage while maintaining responsiveness.

While optimizing DRX parameters for energy savings has been a popular research area (see Section \ref{related}), Adder delves deeper by focusing on maximizing energy efficiency for specific applications like IoT actuators where traffic patterns are infrequent and dynamic. 
In these scenarios, PSM holds significant potential for energy conservation. However, previous approaches either neglected PSM altogether or treated it similarly to DRX, overlooking its distinct energy profile~\cite{zhou2018actor,zhou2019online} . Adder tackles this by: (1) Leveraging real-world energy profile: Adder is designed to adapt to the diverse energy profiles corresponding to different connection states of different devices. (2) Accounting for the cost of wake-up: Unlike previous studies, Adder considers the energy-intensive random access  in the Tracking Area Update (TAU) process required to re-establish network connection upon waking from PSM, providing a more accurate and holistic view of energy consumption.
What's more, while some studies have explored using traffic records from individual devices or aggregated data from devices serving the same service. For individual devices, sparse data collection due to infrequent activity makes accurate prediction difficult. While aggregated data can offer some insights, it often lacks the broader context necessary for service-specific predictions. Adder transcends these limitations by leveraging valuable contextual data from service providers, including weather forecasts, traffic flow information, and holiday schedules. This broader perspective provides a more holistic understanding of the service's traffic patterns, enabling Adder to generate more accurate and service-specific predictions. Imagine ADDER predicting peak demand on sunny weekdays, enabling quick bike unlocks and keeping riders happy. Conversely, during low-demand rainy weekends, it suggests powering down network modules for optimal energy efficiency.

The implementation of Adder presents two significant challenges. Firstly, within the RL framework, there is a substantial issue related to the extensive state and action space, stemming from the combination of high-dimensional contextual factors such as weather and holidays, along with numerous energy-saving settings. Training a model to effectively navigate these intricate spaces demands a substantial amount of data, which is often in short supply in real-world IoT applications. Secondly, there's the limitation of online learning in the RL context, where directly testing different actions in the live system can introduce impractical latency, rendering it time-consuming.
To address these challenges, Adder employs the following strategies. First, it creates a specialized simulator to replicate latency effects under varying traffic arrival probabilities, enabling safe action exploration and data collection without impacting the live system. Second, DDPG framework is employed to handle the high-dimensional action space efficiently. Thirdly, a context-aware predictor is utilized to extract traffic arrival probabilities from contextual data, thereby abstracting the state space and reducing its dimensionality. Consequently, this enables Adder to generate synthetic data that represents a wide range of traffic arrival scenarios, allowing the RL model to train and learn effectively, even when confronted with limited real-world data. By doing so, it successfully overcomes the challenge of data scarcity prevalent in IoT applications 

To assess the effectiveness of Adder, we carried out a comparative examination, pitting it against two baseline approaches: EDRX, configured with an off duration calibrated to match the maximum delay tolerance, and AC-DRX, a solution that employs an actor-critic framework to address similar challenges. The findings demonstrate that Adder outperforms its counterparts in terms of energy consumption efficiency especially in scenarios with infrequent traffic. Furthermore,  the threshold of traffic arrival probability provides service companies with a clear and actionable metric to adjust their energy consumption based on their specific needs and priorities, without needing to delve into the intricacies of individual settings. To further explore the practical implications of Adder, we conducted a case study within the context of bike-sharing. This analysis examined the potential impact of prediction errors arising from the context-aware predictor. The findings provide valuable insights for real-world deployment and can guide further refinement of the prediction model for improved accuracy and reliability.

The remainder of this paper is structured in the following manner: 
Initially, Section~\ref{back} provides a foundational understanding of 
Radio Resource Control (RRC) energy-saving configuration procedures and related parameters. 
This is followed by Section~\ref{meth}, which outlines the architecture of  Adder, 
explaining how Adder participates in the parameters configuration,
and offer an in-depth explanation of 
the reinforcement learning problem we address, 
as well as the intricacies of the traffic pattern estimator.
Finally, an empirical evaluation of our proposed methodology 
is presented in Section~\ref{ev}.





\iffalse
We specifically focus on dynamically adapting parameter settings associated
with energy-saving mechanisms, i.e., DRX and PSM, in NB-IoT and LTE-M. 
To allow service specificity to be considered, \Name{} makes
use of data from the mobile network operator \textit{and} data associated with
the particular service provider or other relevant third party data
(e.g., from the organization operating the
waste management or bike sharing-service, or data concerning weather conditions).
This unique collaboration
capability allows \Name{} to adapt, optimize and tune the energy/latency tradeoff
according to the specific (customer) service needs. 
We use a deep deterministic policy gradient (DDPG) algorithm~\cite{lillicrap2015continuous}, 
deployed "within'' the mobile network, to adapt RRC parameter settings to the specific IoT
service using features extracted by a neural network predictor. 
\Name{} assumes and exploits emerging open and programmable (software-defined)
network architectures in both the radio access network
(RAN)~\cite{oran,flexran} and the mobile core network~\cite{onap,proteus}
to enable ML-based computation and parameter settings to be introduced into IoT
protocol exchanges. Indeed, to our knowledge, \Name{}
is one of the first end-to-end use cases illustrating the need for and
the utility of coordinating software-defined actions across both the
RAN and the mobile core network.

We validate and evaluate our approach
by implementing \Name{} in the ns3 simulation environment. We
evaluate the feasibility of our ML algorithms to adapt and optimize
RRC parameter settings and specifically to provide a tradeoff between 
energy savings and responsiveness. For our evaluations, we make use of
data from a shared mobility service from the Transportation Department 
in Austin Texas. 


%\textit{Maybe still need to cover: (i) Motivate ``in-network'' approach compared
%client based. (ii) Argue why we need these specific ML approaches.
%But maybe these are covered elsewhere already?}



\begin{itemize}
\item As an approach to dynamically configure RRC parameters for IoT applications, 
\Name{} takes both downlink and uplink into consideration. 
The challenge lies in the following three aspects.
First, there is a tradeoff between power consumption and delay for downlink traffic.
Second, communication protocols handle uplink and downlink differently, leading to a situation where the uplink's arrival may impact the downlink's delay.
Third, traffic associated with real applications does not necessarily follow a specific distribution or combination of distributions. 
The traffic distribution can be very dynamic but is still specific to the specific service type.
%These three things add up to make it harder to predict traffic in advance and configure accordingly. 

\item Previous research predicts traffic patterns without any contextual information other than historical traffic records.
As a network side solution based on the SDRAN framework, however, \Name{} 
can utilize extra features to provide superior performance with machine learning algorithms.
We compare the traffic pattern prediction accuracy of \Name{}'s neural network predictor 
with other time-series-based algorithms, and the results indicate \Name{}'s superiority.

\item To illustrate the feasibility of our approach, we implement our algorithms in the ns3 simulation environment 
and evaluate it with bike-sharing data from the Austin Transportation Department.
Furthermore, we evaluate the effect of our reinforcement learning (RL) based approach to setting RRC parameters for different communication scenarios, e.g., periodic, event-triggered etc.   
%
%since the RRC parameter’s updating procedure decides when action occurs affecting reward calculation in the reinforcement learning (RL), 
%we compare the performances of different event-triggered TAU schemes. 
\end{itemize}


%We present the \Name{} architecture which leverages emerging
%software-defined RAN and core network functionality to allow for
%the dynamic adaption of RAN RRC
%parameters based on machine learning algorithms.
%\item \Name{} makes use of data from both network operators and their
%IoT service provider customers, which offers a unique cooperation
%capability, allowing for service specific optimization and tuning 
%of RRC parameters according to the unique tradeoffs required by the IoT
%service.

\fi




\iffalse
As a kind of carrier of data, mobile communication has proliferated to keep pace with the demands for the dramatic rise of data traffic. The market is expanding fast; new services are emerging, which drives mobile communication technologies. Even though a higher data-rate is an eternal goal in every generation of mobile communication systems, it is not the only purpose in this new generation. 4G extend and 5G aim at offering diversified services for various service demands, including low data-rates, massive connectivity, ultra-reliability, and low latency. Customers expect user-friendly, easy-to-deploy tools to configure mobile networks based on their service requirements, improve their service quality, and reduce operating costs\cite{Make5G}.

The Internet of Things, which enables seamless communication among massive end devices, is one of the new services mentioned above.  Many Low-power wide-area technologies have emerged to serve hundreds and thousands of heterogeneous smart IoT devices with relatively lower data rates, longer battery life, and broader coverage area requirements. In 3GPP, Machine Type Communications (mMTC) protocols like NB-IoT and LTE-M are to solve the capacity and energy challenges that prevent typical mobile network protocols from broadly using in massive device scenario~\cite{park2020earn}. 

Discontinuous Reception (DRX) and  Power Saving Mode (PSM) are two mechanisms designed for keeping end devices energy efficient by reducing UEs' listening for downlink control information when there is no data transmission. The mechanisms mitigate and ameliorate a series of problems like battery lifetime, manual battery replacement. Though intended to turn the LTE protocol into one suitable for IoT applications that require less energy consumption, their practical usages can only fulfill sensor-based IoT applications with low latency tolerance. Turning off the radio saves energy while leads to longer latency for inactive UEs to receive paging messages. Network configuration strategies and approaches are the keys to solve the problem. "Dummy" end devices will not be smart enough to arrange lower layer network configurations, and application developers, though, can give instructive and reasonable configurations, the real traffic of an application with high flexibility and freedom is usually beyond their understanding. Only adequate knowledge of the traffic and network situation can lead to a strategy balancing the tradeoff between latency and energy. Many eNBs now support following the suggested DRX and PSM parameter settings from the UEs' sides. However, it is neither promising that an optimal setting would be put forward nor realistic to follow UEs' configuration suggestions. An improper configuration may cause massive end devices access to the network and result in the overload of radio access networks (RANs) and paging storm as the result of limited paging capacity~\cite{10.1145/3395351.3399347}. For NB-IoT that covers one RB, the paging capacity would be less. Sensor devices only report the measurements and do not need paging, which may reduce the paging blocking. Nevertheless, it is still an issue for actuator devices. More collaborations between network operators and partner companies are needed to fulfill various services' demands and reasonably schedule network parameters. 
\fi

\iffalse
Other than focusing on upgrading networks with higher speed only, 4G extend and 5G aim at offering diversified services for various service demands, including low data-rates, massive connectivity, ultra-reliability, and low latency. The Internet of Things, which enables seamless communication among massive end devices, is one of the new services. Many Low-power wide-area technologies have emerged to serve hundreds and thousands of heterogeneous smart IoT devices with relatively lower data rates, longer battery life, and broader coverage area requirements. In 3GPP, Machine Type Communications (mMTC) protocols like NB-IoT and LTE-M mainly tackle the capacity and energy challenges that prevent typical mobile network protocols from broadly using in massive device scenario~\cite{park2020earn}. 

Discontinuous Reception (DRX) and Power Saving Mode (PSM) are two mechanisms for keeping end devices energy efficient by consuming less on the downlink monitoring. They mitigate and ameliorate a series of problems like battery lifetime, manual battery replacement. Though intended to turn the LTE protocol into one suitable for all IoT applications, the mechanisms' practical usages are sensor-based applications only. Turning off the radio saves energy while may lead to paging blocks for inactive UEs. Network configuration strategies and approaches are the keys to solve the problem.

A key design question for RRC parameter settings is where to place the functionality of calculating the optimal parameter set under limited network resources. On the one hand, " Dummy" end devices are smart enough to arrange lower layer network configurations. The battery supply cannot support any extra data processing power. Though application developers can give instructive and reasonable configurations, the network's situation is beyond their control. On the other hand, it is neither promising that an optimal setting would be put forward nor realistic to follow UEs' configuration suggestions since eNodeBs also expect efficient parameter settings for relieving the CPU processing limitation. An improper configuration may cause massive end devices access to the network and result in the overload of radio access networks (RANs). More collaborations between network operators and partner companies are needed to fulfill various services' demands and reasonably schedule network parameters. 

Without cooperating with network operators, companies cannot ensure their service quality. There are only limited options for general purposes offered by network operators. Although network operators are not ready for these emerging services, techniques are developing fast. Network slicing has appeared in many high-level conceptual papers for several years, providing more straightforward personalized solutions for business partners and meeting end-users' SLAs. A set of logical networks serve different business purposes on top of a shared infrastructure to fulfill service differentiation. Now SD-RAN platforms like OpenRAN, O-RAN, and FlexRAN provide tools to enable applications to control and manage RAN within network operators' frame. It inspires us to develop RRC parameter reconfiguration solutions using these platforms' tools for different IoT scenarios. Compared to letting IoT application developers design the parameters without enough information on their users' behavior, these SD-RAN platforms' agents can monitor data flows; controller masters can analyze each UE's traffic pattern and give better predictions and parameter settings.


%\begin{figure}[htp] 
%    	\centering
%    	\includegraphics[width=6cm,height=6cm]{figs/intro.png}
%	\caption[]{not ready}
%\label{intro}
%\end{figure}

Traffic patterns of an application can be extracted by monitoring the traffic. IoT applications' traffic characteristics are most regular, and even those irregular traffic would follow some objective laws. Statistical and artificial intelligence-based approaches can address the forecast for predictable traffic patterns. Fig. \ref{intro} shows the difference of traffic patterns inter-application and intra-applications, which makes it evident that the performance of DRX and PSM parameter configuration substantially varies with the traffic load and RAR procedure timing. More importantly, the underlying model capturing this behavior is highly non-linear and far from trivial. Thus we resort to reinforcement learning methods that adapt to the actual users' behavior, traffic loads. Our contributions are as follows:

\begin{trivlist}

  \item We give a detailed framework on leveraging SD-RAN to adapt RRC parameters with machine learning methods on the network side, which has never been discussed before.
  
  \item The framework offers an interface for the partner companies to input extra features and data to improve the machine learning's outcomes.
  
  \item  Our framework also supports partner companies with a tunable QoS level in case of the new demands as a result of any application functions updates.
  
  \item  We conduct experiments with real bike-sharing application data to show how bike-share companies can cooperate with network providers on tuning the network configurations. 
  
\end{trivlist}

(To be modified)The remainder of the paper is organized as follows. Section 2 provides background information about the RRC state machine and some related procedures. The design principles include architecture; the relationship between RRC parameter settings and IoT traffic patterns are described in Section 3. IoT use cases that require both energy-saving and short-latency are given in Section 4 to indicate such demand is not for a particular case. A detailed bike-share case is analyzed later to discuss how network parameters can improve the service with lower energy costs. Section 5 revises the ML-based configuration strategy and its performance applying to the bike-share case. Section 6 concludes the paper. 
\fi

\iffalse
\textbf{Assumption:} This paper analyses the LPWA technologies standardized by 3GPP up to Release 13, focusing on the enhancements introduced in Release 13 for better support of M2M/IoT services
\fi


\iffalse
As a concrete example, which we explore in detail in this paper, consider the
flow of messages involved in a bike-sharing service: When not in use by a customer,
a bike will periodically send messages to the bike-sharing service to report its
current location. These messages originate from the device, and as described above,
energy-saving methods do not impact the flow of information, i.e.,
here the IoT device might enter into long energy saving periods.
However, when a customer selects a currently unused bike,
the message flow with regard to the IoT device is effectively ``from the network''.
I.e., a bike-sharing app on the user's mobile device will interact with the
(cloud-based) bike-sharing service (to pay for the service), after which the bike-sharing 
service sends an ``unlock'' command to the IoT device on the bike.
To ensure reasonable response times for a user waiting to use the bike,
this actuator action needs to happen in a timely manner, thus creating
tension with the desire to remain in an inactive state to reserve energy
use. This suggests that, for a particular IoT service, 
\textit{different conditions require different parameter settings} 
to balance the tradeoff between energy savings and response time.
Further, the optimal parameter settings and the
relevant tradeoffs between energy savings and latency will be \textit{different
for different IoT services}, e.g., equipping dumpsters with sensors as part of a
smart waste management program
%~\cite{iot_smart_city_use_cases} 
will have a radically different tradeoff from a bike-sharing service.

Given this context, the key question we are exploring is \textit{whether it
is feasible to \textbf{learn} the dynamics of a specific actuator application 
service and use that information to dynamically set the parameters of the
energy saving mechanisms in IoT protocols so as to meet the specific
tradeoffs needed by the service.}
\fi


\iffalse
The number of Internet-of-things (IoT) applications, services and devices continue to grow
unabated making it a key use category for existing and future cellular network
offerings. Massive machine type communication (mMTC) is one of the three
service categories targeted by emerging 5G mobile network services and a
recent report suggests that there will be 14.7~billion M2M connections by 2023, which
will account for half of the global connected devices and connections~\cite{cisco_report_2020}. 
\fi