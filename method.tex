\section{Methodology}
\label{meth}
\subsection{Problem Formulation}
Conserving energy in an IoT actuator needs accurate prediction of packet (paging request) arrival times within a time window to ensure responsiveness to anticipated requests while transitioning to sleep mode during periods of inactivity. However, due to the unpredictable nature of burst traffic, the actuator must carefully balance entering sleep mode for energy savings with frequent monitoring to avoid missing packets. This challenge can be frame into a bandit problem, where decisions are made without full knowledge of the environment, akin to choosing between sleep and monitoring without knowing the exact timing of packet arrivals.
Although precise request time are unattainable,  estimating the probability of a packet arriving within a given time window is feasible. With such a probability estimation, one can set a threshold to omit infrequent requests with low arrival probabilities. When the request probability is moderate, reduced monitoring suffices; however, for high probability scenarios, frequent checks or ensuring delay tolerance are inevitable. Historical data proves valuable in forecasting these probabilities, with time series analysis revealing busy and quiet periods. Apart from time-centric factors, external conditions also play a role. The influence of days of the week, holidays, and local events cannot be ignored in shaping these patterns.  For instance, fields may not require irrigation following prolonged rain, and areas experiencing shared-bike shortages often see increased demand.

\subsection{\Name{} Design}

Adder addresses the energy parameter setting challenge, which is framed as a contextual bandit problem, by employing a modified DDPG algorithm. DDPG stands out due to its deterministic policy, which directly selects the optimal action for each state. Moreover, its "deep" neural network architecture effectively handles the high-dimensional action spaces that involve numerous energy-saving parameters. This sets it apart from AC-DRX, another actor-critic framework, which relies on tabular learning and stochastic policy gradients. While AC-DRX reduces the action space dimension to a single parameter through redefinition, it still necessitates sampling from a distribution encompassing all discrete actions and additional search efforts to find optimal actions. To train the critic and actor networks,  DDPG needs training data, which can be challenging for IoT actuators receiving infrequent commands. To address this data scarcity, Adder decouples the DDPG model from specific application contexts. It accomplishes this by employing a simulator to generate training samples, encompassing varying packet arrival probabilities. When configuring parameters for a specific application, a context-aware predictor estimates the packet arrival probability based on the given context. Subsequently, the DDPG model utilizes this estimated probability to make informed action selections. Using synthetic datasets enables DDPG to undergo effective training without the reliance on an extensive volume of real-world data.




%To address the energy parameter setting problem formulated as a contextual bandit problem, Adder adopts a modified DDPG algorithm. DDPG stands out due to its deterministic policy, which directly prescribes the optimal action for a given state and its ability at dealing with high dimensional action space. AC-DRX, though also a actor-critic framework, it is based on a tabular learning, employs a stochastic policy gradient approach, resulting in a distribution over all possible actions where extra time is need for finding the optimal action, thus they have to  redefine the actions to be taken to effectively simplifying the energy-saving parameters into more manageable metrics.
%Moreover, the `deep' in DDPG indicates its use of deep neural networks, enabling the model to extract intricate and abstract features from high-dimensional datasets. While DDPG can effectively manage large action spaces, it  still requires training data to learn policy. This can be a challenge for IoT actuators, which rarely receive commands. To address the challenge of limited training data, we decouple the DDPG model from the specific contexts tied to a particular application. We employ a simulator to produce training samples with varying packet arrival probabilities. When it comes to choose parameters for a specific application, a context-aware predictor calculates the probability of packet arrival based on the provided contexts. The DDPG model then makes action selections based on this estimated probability for the application. This approach allows the DDPG model to be effectively trained using a dataset composed of synthetic data, reducing the reliance on a large number of real-world training samples. This, in turn, enhances the DDPG model's ability to generalize and be applied to various applications without the need for retraining.

%DPG is more efficient because it uses a critic network to estimate the expected reward for each state and action pair instead of .
\subsection{\Name{} Architecture}
\begin{figure}[htbp]
\centerline{\includegraphics[width=\columnwidth]{figs/overview.pdf}}
\caption{\Name{} Architecture}
\label{overview}
\end{figure}


Figure ~\ref{overview} shows how \Name{} works and how it fits into the standard data transmission process between base stations and IoT actuators. Adder's Learning and Control Application consists of two key components: a context-aware predictor and a DDPG-based policy maker. The predictor, using information from IoT servers and external sources, estimates the likelihood of a data packet arriving from the server in each time window. Meanwhile, a simulator generates training data for the policy maker by simulating traffic with varying arrival probabilities within a time window. This simulation records the resulting latency and energy consumption given the device power profile, DRX and PSM parameters, and service latency requirements. Following the model's training, the policy maker utilizes the predicted probability in conjunction with a predefined threshold specific to the service to determine optimal parameter settings. These newly determined parameter configurations are then transmitted to the controller, where a parameter update process is initiated to ensure that both the network and end devices are effectively prepared for the impending changes.
The updated configurations are included in a TAU response. When the network receives a TAU request from the end devices, it sends them the updated settings. This allows the devices to adjust their local parameters accordingly. End devices can start the TAU request through periodic updates or utilize the on-demand TAU. 
While reconfiguring parameters through TAU introduces overhead in control signaling, the infrequent downlink packets (a quarter hour to serval hours) characteristic of IoT actuator-centric applications makes the overall signaling overhead manageable.

\subsection{Context-aware Predictor}
Accurately forecasting application traffic remains the primary challenge in setting power-saving parameters. Time series analysis, a common technique employed in previous studies, typically concentrates on the traffic patterns of individual devices.  Even when applied to forecast the traffic pattern for a service, it may not fully capture the infrequent, unpredictable, and intermittent nature of IoT actuator applications. Consider a bike-sharing service as an example, where usage patterns can fluctuate significantly based on the time of day, day of the week, weather conditions, and specific events. Even with a consistent usage pattern, the traffic pattern of each bike is likely to differ. While basic time series data might be adequate for tracking daily peaks, identifying variations across weekdays versus weekends or comprehending seasonal and annual shifts—such as those observed during pandemic years—requires more training data which may be impractical. By integrating more "context" from the environment, \Name{} can craft more precise traffic forecasts. This enables finer power-saving strategies and bolsters the efficiency of IoT actuator applications. In certain scenarios detailed in Section~\ref{ev}, our case study with a bike-sharing service revealed that \Name{} surpassed conventional time series forecasting models like ARIMA and LSTM in predicting user demand patterns.


\subsection{Modified DDPG}
We consider the problem under the scenario that an agent operates within an independent and identically distributed (i.i.d) contextual bandit framework that allows for continuous action choices.
At each time step, denoted as $t$, and gathers a state vector $x_t =[x_{t1}, x_{t2} ]$ from the state space $\mathcal  X = [0, 1]^2$.
The completion of each time step is marked by a TAU update. 
At the beginning of each step, the new energy-saving parameters for the step set up, and the timer for the periodic TAU is reset. If no requests are received before the timer runs out, the step terminates with the periodic TAU. If a request arrives before the timer reaches zero, the UE triggers an on-demand TAU, which completes the current step.
$x_{t1}$ is the estimated packet arrival probability.
$x_{t2}$ stands for a threshold determined by the IoT service provider. 
When $x_{t1}$ falls below this threshold, energy considerations become paramount; 
conversely, if $x_{t1}$ surpasses the threshold, latency takes precedence. 
Following this, the agent selects an action $a_t $ 
from action space $\mathcal A$, where  $\mathcal A = [-1, 1]^N$ 
and receives reward $r_t = \mathcal  R(x_t, a_t)$, where $\mathcal  R: \mathcal X \times \mathcal A$. 
To simplify the problem, we leave out the settings for cDRX and PTW, 
only focus on the configurations of $T_{3324}$, $T_{eDRX}$ (eDRX cycle), and $T_{3412}$, which results in $N = 3$.
We modify the values of $T_{3324}$ and $T_{3412}$ in terms of TTI units (equivalent to the duration of a subframe), $T_{eDRX}$ is expressed in units of 10 TTI (equivalent to the duration of a radio frame).  These values can vary from 0 up to the maximum TTI in the time window of a single step and are mapped onto the action space respectively. These values were originally set as fixed values in accordance with the 3GPP protocol.

The primary objective for Adder is to prioritize meeting delay tolerance requirements 
during service peak hours while concurrently aiming to 
maximize energy conservation during off-peak periods. 
Thus we define the reward function $\mathcal R$ as follows:
\begin{equation}
 \mathcal R =
    \begin{cases}
      D_{a_t} + L_{a_t} &\text{if $x_{t1} > x_{t2}$, $S > d$}\\
      (1-D_{a_t}) + E_{a_t} +L_{a_t} & \text{if $x_{t1} > x_{t2}$, $S <= d$}\\
      \alpha D_{a_t}+ E_{a_t}+ L_{a_t} & \text{if $x_{t1} <= x_{t2}$}
    \end{cases}       
\end{equation}
where $d$ represents the delay tolerance, a variable contingent 
on the specific requirements of the IoT application and 
$S$ is the maximum delay achievable by the selected action set.
$E_{a_t}$ and $L_{a_t}$, representing the normalized energy cost and normalized actual latency cost, respectively. 
$D_{a_t}$ denotes the normalized distance between the maximum delay of the selected action set and the delay tolerance, i.e. $d-S$. 
The coefficient $\alpha$ is a control parameter that can be used to specify
the relative importance of the maximum delay cost over energy cost 
depending on the IoT application requirements.


$D_{a_t}$ serves as a mechanism for constraining action selection during peak hours. 
It imposes a penalty in the form of a negative reward 
when an action set chosen results in a waiting time exceeding the specified delay tolerance.
This penalty is essential for preventing the model
 from overly prioritizing energy savings in scenarios 
 where the probability of a request arrival is low 
 but still categorized as peak hours according to the IoT application's requirements. 
 It ensures that the model maintains latency within the delay tolerance.
Conversely, $D_{a_t}$ grants positive rewards for actions 
when the maximum delay incurred by the action set closely approaches 
but remains below the delay tolerance. 
This reward system discourages the model from becoming excessively focused on 
minimizing latency costs to the detriment of other factors.
The coefficient $\alpha$ is a control parameter that can be used to specify
the relative importance of the maximum delay cost over energy cost 
depending on the IoT application requirements.
This approach may result in longer wait times during off-peak hours, 
potentially leading to the loss of some customers. 
Businesses have the flexibility to adjust their threshold $x_{t2}$
to strike a balance between device maintenance and customer profitability, 
striving for maximum overall gain.

The process is then repeated with a new state at time $t + 1$. 
Unlike the standard RL setting, there is no transition function in the bandit setting.
We defined  deterministic policy $\mu_\theta$ that maps states 
to specific actions in $\mathcal A$, parameterised by $\theta$.
A deep contextual bandit agent for continuous actions based upon the DDPG algorithm \cite{duckworth2022reinforcement}
is used in \Name{} to help deal with the one step RL problem. 
DDPG is a model-free, off-policy reinforcement learning algorithm 
designed specifically for environments with continuous action spaces. 
Combining concepts from Deep Q-Learning (DQN) and Actor-Critic methods, 
DDPG utilizes two neural networks: an actor network that determines the optimal action given a state, 
and a critic network that evaluates the quality of a given state-action pair. 
The actor produces a deterministic policy, guiding the agent towards the best perceived actions, 
while the critic estimates the Q-value of the chosen action, helping to refine the actor's decisions. 


Usually, as a solution for MDP problem, 
Q-value represents the expected cumulative future reward an agent can obtain, 
starting from state s, taking action a, and thereafter following a specific policy and 
is described and updated using the Bellman equation. 
As a bandit problem, the selected action won't affect the future state, 
thus the optimal one-step Q-value for $(x_t, a_t)$ as follows: 
\begin{equation}
Q^*(x_t, a_t) = \mathbb{E} [\mathcal R(x_t, a_t)],
\end{equation}
We refine the critic network using the following mean-squared error (MSE) function to bring our estimated Q-values $Q_\phi$ closer to the optimal $Q^*$.
\begin{equation}
L(\phi, \mathbf{E}) =\operatornamewithlimits{ \mathbb{E}}_{(x_t, a_t, r_t) \sim \mathbf{E}} \left[ \left( Q_\phi (x_t, a_t) - r_t \right)^2 \right],
\end{equation}
where $\phi$ represents the critic network parameters.
Given that our focus is solely on the immediate rewards of each step, rather than the cumulative reward, we removed the target networks. 

Since the actor network produces a deterministic action for each state, 
a behaviour policy $\beta$ is used for exploration, by adding noise sampled from a one-step Ornstein-Uhlenbeck process $\mathcal N$ to the deterministic actor policy:
\begin{equation}
\beta = \mu(x_t | \theta_t^\mu) + \mathcal N,
\end{equation}
We update the deterministic policy gradient for the contextual bandit setting by:
\begin{equation}
\begin{aligned}
	\nabla_\theta \mathbf J_\beta (\mu_\theta) &=   \int_{\mathcal X}\rho^\beta \nabla_\theta \mu_\theta (a | x) Q^\mu(x,a) ds, \\ 
	&= \mathbb{E}_{x_t \sim \mathbf{E}} \left[ \nabla_\theta \mu_\theta (x_t) \nabla_a Q^\mu (x_t, a_t) |_{a_t = \mu_\theta (x_t)} \right]	,
\end{aligned}
\end{equation}
where, $\rho ^\beta (x)$ defines the state distribution visited under policy $\beta$, which in the bandit setting is equivalent to sampling states from our environment $\mathbf{ E}$.

