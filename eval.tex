\section{Evaluation}
\label{ev}

\subsection{Simulator}
We use a simulator to generate the data samples needed to train our model. The simulator creates packets based on a random probability of packet arrival within a given time window. It precisely mimics the behavior of UE as it transitions between different states according to an energy-saving mechanism when handling downlink packets and we record the energy consumption and resulting latency for training. The energy consumption in the simulator is calculated based on the specifications from the nRF9160 by Nordic Semiconductor, which is a System-in-Package (SiP) that facilitates low-power cellular IoT designs with its modem support. This SiP is compatible with both PSM and eDRX power conservation techniques. The table \ref{tab:profile} provides the power characteristics of the nRF9160 rev2 chip operating in NB-IoT network mode with a voltage of 3.7v. To simplify the simulation,  we make the assumption that during each TAU from PSM, we switch to the RRC connection state for 2 seconds. It's important to note that during this time, no data transmission occurs, and the current usage is equivalent to that of the RRC inactivity state. Our simulation environment is built on Gymnasium's APIs~\cite{towers_gymnasium_2023}. While there is an ns-3-based model for LTE energy conservation, ns-3's extensive components tend to slow down data acquisition. Our simulator has significantly improved data collection speed, being approximately eight times faster than the ns-3-based model.


\begin{table}[!t]
\label{tab:profile}
\centering
\caption{Current Consumption Parameters}
\begin{tabular}{l l l}
\toprule
\textbf{Symbol} & \textbf{Description} & \textbf{Value} \\
\midrule
\(I_{PSM}\) & PSM floor current & 2.7\(\mu\)A \\
\(I_{PO}\) & Average current during PO & 5913\(\mu\)A \\
\(I_{eDRX}\) & Average current during idle edrx & 23.82\(\mu\)A \\
\(I_{inact}\) & Average current during RRC inactivity & 15345\(\mu\)A \\
\bottomrule
\end{tabular}
\end{table}

\subsection{DDPG configuration}
Since  target networks are removed from the DDPG algorithm, we only need to configure the actor and critic networks. The actor network is composed of three hidden layers, each employing a ReLU activation function. It culminates in a Tanh activation function in the output layer. The output is then mapped to the appropriate parameter value ranges, and the resultant action values are input into the simulator. These hidden layers consist of 64 units, 32 units, and the dimension of the action space, respectively.
Meanwhile, the critic network utilizes two ReLU hidden layers, each comprising 32 units, to process the observations. It subsequently combines these processed observations with the actions and passes the result through three additional ReLU hidden layers. These hidden layers in the critic network also consist of 64 units, 32 units, and finally 1 unit. During the training process, we employ a batch size of 256, and both the actor and critic networks use a learning rate of 0.0005.

\subsection{Performance}

\textbf{Baseline Methods}
To evaluate our approach, we compare it with two baselines. The first baseline, called EDRX, uses a static DRX cycle matching the delay tolerance parameter. Our second comparison is AC-DRX, an Actor-Critic-based approach that combines tabular V value appraisal as a critic with a stochastic actor policy for improved energy efficiency. 

To compare AC-DRX with ADDER, we need to make some modifications to AC-DRX. First, the original AC-DRX framework defines decision windows (DWs) based on a predetermined number of incoming requests. Each incoming request is associated with a corresponding RRC state, and these states are subsequently used to determine the action to be taken for the next DW. We redefine the DW to be measured by the number of steps (corresponding to each TAU update, as detailed in Section 1) instead of the number of incoming requests. This modification introduces a new state into AC-DRX to account for a case when no packets are received within a step. Furthermore, the action in the original AC-DRX framework is confined to determining the length of the DRX cycle and does not encompass the timing configurations for PSM, which is referred to as the RRC idle mode in their setup. To align with our method's action space for a comparative analysis, we adapted the original AC framework to include the  parameter setting for PSM. This required a transition to a DDPG strategy, which is better suited to our broader action space. For the critic component in AC-DRX, we replaced the tabular approach for estimating the V value for a state with a deep Q-learning algorithm for a state-action pair, while retaining their rule for updating the V value, which utilizes accumulated rewards to inform the Q-learning process. We continue to use their defined reward function to ensure that the model benefits from their established conceptual groundwork.


\textbf{Setup}
In our analysis, we set a fixed periodic TAU period for each step and configure the AC-DRX model with DWs spanning 5 steps each.While the packet arrival probability remains constant during a single DW (five steps for ADDER), it varies across different DWs to replicate the fluctuating peak and off-hour traffic patterns frequently observed in IoT environments.ADDER is provided with the arrival probability and a threshold. This threshold determines the extent of savings achieved when the actual usage probability falls below it. ADDER 10 and ADDER 40 are equipped with thresholds of 10 and 40, respectively. Our primary objective in this section is to assess the degree to which ADDER can outperform AC-DRX and EDRX in scenarios characterized by non-stationary and infrequent traffic. In the following section, we will examine a case study to illustrate how prediction errors can influence the level of improvement.


\begin{figure}
    \centering
    \subfigure[A single packet randomly arrives within a one-hour time window]{\includegraphics[width=0.24\textwidth]{figs/Energy1.pdf}} 
    \subfigure[A single packet randomly arrives within a two-hour time window]{\includegraphics[width=0.24\textwidth]{figs/Energy2.pdf}} 
    \caption{CDF for Energy Consumption across Different Time Window}
    \label{fig:120}
\end{figure}

\iffalse
\begin{figure}
    \centering
    \subfigure[Latency]{\includegraphics[width=0.24\textwidth]{figs/Latency1.pdf}} 
    \subfigure[Energy]{\includegraphics[width=0.24\textwidth]{figs/Energy1.pdf}} 
    \caption{A single packet randomly arrives within a one-hour time window}
    \label{fig:60}
\end{figure}


\begin{figure}
    \centering
    \subfigure[Latency]{\includegraphics[width=0.24\textwidth]{figs/Latency2.pdf}} 
    \subfigure[Energy]{\includegraphics[width=0.24\textwidth]{figs/Energy2.pdf}} 
    \caption{A single packet randomly arrives within a two-hour time window}
    \label{fig:120}
\end{figure}
\fi

\textbf{Results}
Figure~\ref{fig:120} show the performance of the methods, evaluated over time windows of one and two hours per step respectively. The lengths of the time window are pertinent to applications with infrequent traffic, such as actuators receiving commands every few hours. The figures employ Cumulative Distribution Function (CDF) plot for energy consumption analysis, measured in 100-step increments.  
For the one-hour time window, approximately 7.5\% of the samples for the Adder 40 with latencies over 10 seconds. These samples result from the threshold that disregards latencies of packets that are unlikely to arrive, yielding substantial energy savings. Adder 40 predominantly shows energy usage under 30 Joules per 100 steps, which stands out against the other methods that all exceed 30 Joules. Notably, Adder 40 achieves a 26\% reduction in total energy consumption compared to EDRX. Meanwhile, Adder 10 consistently shows latencies under 10 seconds, with an average around 5 seconds and a maximum of 10 seconds, but it only offers a 5.7\% energy saving over EDRX. Both EDRX and AC-DRX maintain median latencies below one second. However, AC-DRX, designed with a focus on energy efficiency, shows a more varied latency range and realizes a energy saving of 2.9\%. 


For the two-hour time window, Adder 40 is more energy-efficient, saving approximately 32.7\% more energy than EDRX, while Adder 10 saves around 5.9\%. Adder 40 exhibits a 9\% occurrence of samples with latency exceeding 10 seconds, whereas all the samples for Adder 10 stay within the latency boundary. We observed that Adder 40 consumed less energy than in a one-hour time window, which results from Adder's tendency to adopt riskier strategies as traffic becomes less frequent.  Adder switches to PSM when the probability is low. When the probability is above the threshold but remains below a certain level, Adder uses a combination of PSM and TAU to save energy.
On the other hand, AC-DRX consumes more energy than EDRX. This is because AC-DRX lacks information about the probability of packet arrival, often assuming that if there's a higher packet arrival rate during the on-duration of a DRX cycle in a DW, it implies a lower probability of packet arrival in the next DW. Consequently, AC-DRX is inclined to enter PSM for extended off durations to maximize rewards while simultaneously increasing the frequency of packet checks during the RRC idle mode to avoid potential penalties on latency. However, this reward function does not consider the variations in energy costs associated with extended sleep periods in PSM, the RRC idle mode, and the energy consumption for Tracking Area Update (TAU) after PSM. This oversight is one of the reasons why AC-DRX performs less effectively.

\subsection{Case Study: Shared Micro-mobility Vehicle  in Austin, Texas}
To showcase Adder's effectiveness, last section we conducted an experiment with non-stationary traffic but provide Adder the packet arrival probabilities. However, in practical scenarios, an estimator is required to offer approximations, which could potentially impact Adder's performance. To explore how traffic prediction might degrade performance, we examined a specific case study involving scooter sharing in Austin, Texas. While scooters have larger batteries and are less concerned about communication energy use compared to bike sharing, the similarities in user habits and environmental influences on usage patterns make this scooter sharing scenario a pertinent model for our investigation .Considering our focus on applications sensitive to energy consumption, we will substitute ``scooter" with ``bike" throughout our analysis to prevent any confusion.

\subsubsection{Data Overview}
Our dataset comprises two components: the usage statistics and the contextual information.
The usage statistics source from the Austin Transportation Department's
``Shared Micro-mobility Vehicle Trips" report~\cite{ATD} related to a scooter sharing service. We selected a subset of data from the West Campus neighborhood in central Austin, where the records from March 2019 to June 2019 are notably comprehensive and consistent.This dataset features details like device ID, census tract numbers for both starting and concluding locations, trip duration, and a 15-minute interval marking the trip's initiation and conclusion (for safeguarding user privacy).
The accompanying contextual data encapsulate local event and weather information. Publicly available data sources provided information on campus happenings, such as school vacations and national holidays.
For weather data, we extracted hourly historical records from Wunderground~\cite{WDG}, narrowing our focus on the Austin-Bergstrom International Airport Station. Located just 9 miles from West Campus, this station offers the most detailed weather records available in close proximity.

The demand-supply ratio can be interpreted as the probability that a bike will be unlocked and thus indicating service high and low periods. It can be derived by dividing the predicted scooter demand by the number of scooters supplied by the service companies. In the absence of data on the exact number of scooters supplied by the service companies, we approximate the total number of scooters supplied by dividing the scooters demand by the average number of times a scooter is used (i.e., turnover rate).The turnover rate is a measure of how often a bike is used. It is calculated by dividing the total number of bike trips by the total number of scooters in the system. A higher turnover rate indicates that bikes are being used more frequently, while a lower turnover rate indicates that bikes are being used less frequently.The turnover rate adopted for our analysis is twice per vehicle~\cite{hua2020estimating}.


\subsubsection{Performance of Context-aware Predictor}
The usage data is divided into training and testing sets. Any missing values are filled in with their respective mean values, and the dataset is subjected to min-max normalization. To assess the stationarity of the usage data, we utilize the Augmented Dickey-Fuller (ADF) test. This test, known as a unit root test, measures the impact of a trend on a time series. The results, which include the p-value and ADF statistics from the ADF test, are presented in Table \ref{ADFtest}. Since the p-value is below 0.05, it confirms that the series is indeed stationary.Therefore, time series-based algorithms can be employed for usage prediction.%(-4.706562370830151, 8.17962500371902e-05, 25, 2902, {'1%': -3.4326053734385766, '5%': -2.86253647269064, '10%': -2.56730045070749}, 25269.138475745418) ADF result


\begin{table}[htbp]
\caption{ADF test results on the time series.}
\vspace{-0.7cm}
\begin{center}
\scalebox{0.7}{
\begin{tabular}{|c|c|c|c|c|c|}
\hline
\textbf{} & \textbf{\textit{ADF Statistic }} &  \textbf{\textit{p-value}} &  \textbf{\textit{Critical Value 1\%}} &  \textbf{\textit{Critical Value 5\%}} &  \textbf{\textit{Critical Value 10\%}}\\
\hline
 \textbf{\textit{Value}} & -4.71 & $8.12e^{-5}$ & -3.43 & -2.86 &  -2.57\\
\hline
\end{tabular}
\label{ADFtest}
}
%\vspace{-0.3cm}
\end{center}
\end{table}



\textbf{The ARIMA Model}
We begin by applying an ARIMA model to the dataset of bike usage and then analyze the residual errors. The PACF plot displayed in Figure \ref{acf} highlights a distinct peak at lag $1$, suggesting nonseasonal behavior. The ACF follows a diminishing trend, pointing to an $AR(1)$ component. This suggests a nonseasonal model component of $(1, 0, 0)$. In terms of seasonal patterns, the seasonal period is defined as $S = 24$. Both the ACF and PACF exhibit significant autocorrelation at lags of $24$ and $48$. A notable grouping is seen around lag $24$ in the ACF. Additionally, the PACF presents peaks at two intervals of $S$, leading us to choose $AR(2)$. Consequently, the seasonal portion of the model is designated as $(2, 1, 0, 24)$.

\begin{figure}[htbp]
\vspace{-0.3cm}
\centerline{\includegraphics[width=9cm, height=4cm]{figs/acf_pacf.png}}
\caption{ACF and PACF}
\label{acf}
%\vspace{-0.3cm}
\end{figure}

\textbf{The LSTM Model}
Many studies employ LSTM for forecasting purposes. In the context of bike-sharing, the time series data is converted into instances having an input-output structure with a lag of 24. The chosen model comprises two hidden layers, each containing 50 neurons. The Adam algorithm is utilized as the optimization method, and the mean squared error (MSE) serves as the loss metric. The set parameters include a learning rate of 0.0008, a dropout rate of 0.2, a batch size of 50, and 800 epochs.

\textbf{Context-aware Predictor}
The \Name{} constructs a Context-aware Predictor utilizing a neural network (NN). Features that influence commuting behaviors, such as hourly temperature, rainfall, wind intensity, holidays, day-of-the-week, and hour-of-the-day, are employed to train this NN. This network, specifically a multilayer perceptron (MLP) regression, consists of three hidden layers with five hidden units in each layer. The Adam algorithm is chosen for optimization, and the ReLU function is used for activation.

To assess the performance of the models, we used three metrics: mean absolute percentage error (MAPE), mean squared error (MSE), and root mean squared error (RMSE). In addition to the standard test set, we evaluated the models on data from rainy days, holidays, and windy days. We selected data for these specific test sets using control variables, filtering out entries with normalized hourly precipitation or wind speeds greater than 0.5. The accuracy results are presented in Table \ref{eval}.
 
 \begin{table}[htbp]
\caption{Results of the MLPRegressor performances for the bike-sharing case with 3 different loss functions.}
\vspace{-0.3cm}
\begin{center}
\scalebox{0.8}{
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
\multirow{2}[1]*{Method}&\multicolumn{3}{c}{\textbf{\textit{Test}}}&\multicolumn{3}{|c|}{\textbf{\textit{Rainy}}}\\
\cline{2-7}
   &\textbf{\textit{MAE}} &\textbf{\textit{MSE}} & \textbf{\textit{RMSE}} & \textbf{\textit{MAE}} &\textbf{\textit{MSE}} & \textbf{\textit{RMSE}}\\
\hline
ARIMA & 14.42 & 359.65 & 18.96   & 27.82 & 1588.60 & 39.86 \\
LSTM & 19.7 & 591.29 & 24.31& 32.592 & 2010.48 & 44.83 \\
NN & 14.27 & 346.92 &18.63  & 21.09 & 899.22 & 29.99 \\
\hline
\multirow{2}[1]*{Method}&\multicolumn{3}{c}{\textbf{\textit{Holiday}}} &\multicolumn{3}{|c|}{\textbf{\textit{Windy}}}\\
\cline{2-7}
& \textbf{\textit{MAE}} &\textbf{\textit{MSE}} & \textbf{\textit{RMSE}} & \textbf{\textit{MAE}} &\textbf{\textit{MSE}} & \textbf{\textit{RMSE}}\\
\hline
ARIMA & 24.59 & 789.10 & 28.09 & 85.59 & 10069.63 & 100.34 \\
LSTM & 20.58 & 500.52 & 22.37 & 21.54 & 731.06 & 27.038 \\
NN & 11.07 & 176.43 & 13.28 &  22.05 & 752.02 & 27.42  \\
\hline
\end{tabular}
\label{eval}
}
%\vspace{-0.3cm}
\end{center}
\end{table}

The Neural Network (NN) model outperforms the other two methods in terms of predictive accuracy on rainy days and holidays, while it performs similarly to LSTM on windy days. Since ARIMA and LSTM are primarily time series models, it is expected that the NN would outperform them in situations that are less time-dependent. This finding is consistent with previous studies on bike-sharing system demand prediction~\cite{10.1145,gebhart2014impact, LDA}, which suggest that both regular (e.g., time and weather) and opportunistic contextual factors (e.g., social and traffic events) play important roles in forecasting bike usage patterns.


\subsubsection{Decision Making based on the Context}

The discrepancy between the anticipated and actual traffic patterns could result in the selection of an alternative action, impacting both energy efficiency and latency. In this paragraph, we demonstrate how this variance in performance plays out using the bike-sharing scenario as an example. Figure \ref{fig:figure4} depicts the performance for Adder act (based on actual packet arrival probability), Adder pred (based on estimated packet arrival probability), and EDRX approaches, respectively. For both Adder act and Adder pred, we have set the threshold at the 30th percentile of the daily packet arrival probability. The latency plot for the three methods reveals that the majority of latency measurements range from 0 to 10 seconds, averaging around 5 seconds. The box plot does not include outliers which shows around 9\% of Adder act samples have latencies over 10 seconds, while about 12.5\% of Adder pred samples exceed this latency threshold, suggesting that prediction errors can increase latency. Regarding energy consumption, both Adder act and Adder pred are shown to be more efficient than EDRX, with the Adder act curve higher than Adder pred, indicating greater energy efficiency when using actual data. The experimental results demonstrate that devices equipped with the nRF9160 NB-IoT module save 14.5\% of energy using actual probability and 12.9\% with predicted probability over an one day period.


\begin{figure}
    \centering
    \subfigure[Latency]{\includegraphics[width=0.24\textwidth]{figs/Latency3.pdf}} 
    \subfigure[Energy]{\includegraphics[width=0.24\textwidth]{figs/Energy3.pdf}} 
    \caption{ Packets generated according to the records from the Shared Micro-mobility Vehicle in Austin, Texas}
\label{fig:figure4}
\end{figure}


% In essence, the Adder strategy demonstrates superior performance over EDRX in both latency and energy metrics, irrespective of whether it is using actual or predicted traffic patterns. The marginal difference between the actual and predicted outcomes for Adder implies a reliable prediction model and minimal performance drop-off due to prediction inaccuracies. This underscores the Adder strategy's resilience in maintaining optimal performance even when based on predictive rather than actual traffic information.
