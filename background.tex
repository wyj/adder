\section{background}
\label{back}
In this section, we provide the necessary background.
We first explain the energy-saving mechanisms, encompassing a variety of energy-saving parameters, 
as well as the procedures for reconfiguring these parameters.
Particular attention is paid to pinpointing the specific parameters that ADDER can optimize for actuator applications. 
Following this, we provide an overview of several IoT actuator use cases, 
alongside an analysis of their respective traffic patterns, 
which serves for the problem formulation in the subsequent sections.


\iffalse
In this section, we give a brief background introduction of all kinds of the RRC timers during state transmission and the procedures to set the timers' values in LTE-M and NB-IoT. We use the legacy LTE terminology of EPCs, eNodeBs, UEs, and RRC state machines with DRX and PSM mechanisms since LTE-M and NB-IoT are enhanced versions of the legacy LTE. Except for the slight difference in the RRC parameter value options, and NB-IoT has its unique and mandatory EPS optimization, they can be regarded as the same within the paper's scope.
\fi

\subsection{Energy Saving Mechanisms and Related Parameters}


\iffalse
Discontinuous reception (DRX) is a technique first used in legacy LTE to prevent end devices from spending extra energy checking downlink channels for incoming data.
As shown in Fig.~\ref{fig-rrc} and Table~\ref{tab-parameters}, in NB-IoT and
LTE-M an extended DRX (eDRX) and a new power saving mode (PSM) were introduced as
additional energy saving mechanisms.
\fi


\iffalse
\begin{table}[htbp]
\caption{Electrical data comparison}
\begin{center}
\scalebox{0.8}{
\begin{tabular}{|c|c|c|}
     	\hline
	\textbf{Timer}               & \textbf{\textit{Mode}} & \textbf{\textit{Description}} \\  \hline
	rrc Inactivity 		&  - 	  & Timer for transit to Idle state after the last transmission\\  \hline
	drx-Inactivity 		&  c-DRX  & Extra "On" time after the reception of a PDCCH\\  \hline
	OnDuration		    &  c-DRX  & Time spent in active listening \\  \hline
	drx-Retransmission  &  c-DRX  & Time for retransmission \\ \hline
 	cDRXcycle           &  c-DRX  & Time interval between listening periods \\  \hline
	DRX start offset    &  c-DRX  & Specifies the subframe the DRX cycle starts. \\  \hline
	eDRXcycle           &  I-eDRX & interval between two PTWs\\  \hline
	PTW   		        &  I-eDRX & duration of a PTW\\  \hline
 	T3324               &  PSM    & duration of I-eDRX in Idle state\\  \hline
	T3412               &  PSM    & Interval between two TAUs \\  \hline
\end{tabular}
}
\label{tab-parameters}
\end{center}
\end{table}
\fi

The RRC layer functions as the central controller, 
responsible for managering radio communications between the mobile device and the network.
In terms of energy management, the RRC layer plays a critical role by negotiating 
with mobile devices to establish an agreement on the sleep and awake time interval. 
Following the time schedule, mobile devices shift between different  RRC states, 
which in turn governs their radio activity. 
This process is crucial for balancing energy efficiency 
with the need for consistent and reliable communication.

\textbf{DRX and PSM Mechanism}
DRX allows devices to enter a low-power state while periodically checking for incoming traffic, 
necessitating maintenance of network synchronization and readiness for immediate wake-up and data reception. 
On the other hand, PSM puts the device into a deeper sleep, shutting down more functionalities, 
completely disconnecting from the network, and achieving greater energy savings. 

\textbf{Energy Saving Parameters}
Fig.~\ref{fig-rrc} illustrates the relationship between radio
activity, RRC states and parameters for DRX and PSM.
%  and Table \ref{tab-parameters} lists the timers alongside the
% operational mode they are used in and a brief description of their functionality.
Data transmissions occur in the RRC connected state.
Following data transmission, an RRC inactivity timer is initiated to accommodate potential subsequent data transmissions.
To save energy during the RRC inactivity timer, 
A DRX Mechanism in RRC connected state called cDRX were introduced, 
whose configurations includes mainly the OnDurationTimer, and cDRX cycle.
%In 3GPP release~14, both NB-IoT and LTE-M may use a Release Assistance
%Information (RAI) flag to request the immediate transition to the idle state,
%after the data transmissions, saving energy at the cost of reachability and
%delay in case another message needs to be transmitted shortly afterwards.
In the RRC Idle state and PSM, critical parameters such as Paging Frame (PF), Paging Occasion (PO), 
Paging Time Window (PTW), eDRX cycle, and T3324 play vital roles in balancing device availability and energy conservation. 
PF determines the specific time periods for the device to wake up and check for paging messages,  
while PO specifies the exact instances within these frames, enhancing energy efficiency. 
The PTW defines a sub-duration within the DRX cycle for receiving paging messages, offering additional power savings. 
The eDRX cycle extends these wake-up intervals, allowing for prolonged low-power states and substantial energy conservation.
Lastly, T3324 manages the transition duration from active to idle states, 
preventing unnecessary state changes and further contributing to power efficiency.
The time span following the expiration of Timer T3324 up until Timer T3412 runs out is referred to as PSM. 
At the end of this period, as marked by Timer T3412's expiration, 
UEs send TAU requests to reconnect to the network.

\begin{figure} [htp]
	\centering
	\vspace*{-0.5cm}
	\includegraphics[width=9cm, height=5cm]{figs/RRCstatemachine.png}
	\vspace*{-0.7cm}
	\caption{RRC states, radio activity and timers for NB-IoT and LTE-M.
	{\small(While there
	are small differences between the RRC parameter options and NB-IoT has unique and
	mandatory EPS optimization, the diagram applies to both protocols within the context
	of \Name{}.)} }
	%\vspace*{-0.3cm}
\label{fig-rrc}
\end{figure}

Traditionally, RRC parameter values are initially determined during the first RRC setup, followed by negotiations between UEs and MMEs to refine these settings.These negotiations employ specific Information Element Identifiers (IEIs) for the aforementioned parameters during RRC Connection Reconfiguration and TAU procedures. However, the introduction of Early Data Transmission (EDT) in 3GPP Release 15 has eliminated the need for conventional RRC Connection Reconfiguration, favoring an event-triggered TAU instead. This approach offers significant advantages in specific scenarios, such as low battery situations or unique IoT application requirements. In Section~\ref{meth}, the workflow depicts how ADDER leverages event-triggered and periodic TAUs for timely reconfiguration of energy-saving parameters.

\iffalse
\subsubsection{\textbf{Connected state}}
Frequent data transmissions happen and Extended Discontinuous Reception (eDRX) is applied to c-DRX  in this state.

\textbf{RRC Inactivity timer.}
This timer restarts whenever there is a Mobile Originated/MobileTerminated (MO/MT) data transmission. Once the timer expires, eNodeBs release the connection and UEs enter the idle state. This timer's value can be set to zero using the Release Assistance Indicator (RAI) flag by UEs.


\textbf{c-DRX.}
To save energy during the connected state, UE alternates between high energy periods of listening for scheduling information and low energy periods of sleeping which is called c-DRX. c-DRX configurations includes the OnDurationTimer, drx-Inactivity timer, drx-RetransmissionTimer, long c-DRX cycle and DRX start offset. LTE-M and NB-IoT discard the original short c-DRX cycle in the legacy LTE and extend the c-DRX cycle. The network informs UEs of c-DRX configurations in the RRC ConnectionReconfiguration or RRC Connection Setup messages.

\subsubsection{\textbf{Idle state}}
In the idle state, the UE can utilize two power-saving mechanisms: Extended Discontinuous Reception (eDRX) and Power Saving Mode (PSM). The network and the UE need to negotiate the parameters settings.

\textbf{I-DRX.}
Similar to c-DRX, but with more sporadic listening periods. An I-DRX cycle corresponds to a sequence of DRX listening/sleep cycles, called Paging Time Window (PTW), followed by an extended sleep period. Extended discontinuous reception (eDRX) is a power saving optimization feature introduced in 3GPP Rel. 13.  It supports a longer DRX cycle than the legacy DRX power saving features. In this paper, we only take I-eDRX into consideration. The eDRXcycle length and PTW parameter determines the Paging Hyperframe (PH), Paging Frame (PF) and Paging Occasion (PO) in which the eNodeBs send paging messages.

\textbf{PSM.}
PSM enables end devices to enter a deep sleep mode where they can turn off the radio for an extended period but still keep their registrations to the network. Only a periodic Tracking Area Update (TAU) procedure or an uplink data transmission can wake up the connection. PSM is characterized by two timers, namely T3412 and T3324. The T3412 timer triggers TAU, and the timer T3324 is the duration of eDRX. After that, the UE is no longer reachable by the network.
\fi


\iffalse
\paragraph{Paging Procedure and Blocking}
The latency brought by the aforementioned energy-saving mechanism is mainly from paging blocking.
Fig.~\ref{paging} depicts how the paging block happens.
The arrival of downstream traffic for a UE in an idle state at the core network
(the evolved packet core (EPC))
triggers the MME to send S1 paging messages to all the eNodeBs in the
tracking area list associated with the UE.
eNodeBs receive the S1 paging message from the MME, slot the paging request
to a list for a particular PO, and broadcast the list. The target UEs fetch
the paging lists during the POs as planned (i.e., when they switch on their radios
to monitor the downlink control channel), launch random access and service requests
to create bearers, and receive the packet flows.
If, however, the UE does not detect the paging request from the network, i.e., if
\textit{paging blocking} occurs, the UE will not activate to enter connected state,
thus resulting in increased delay. 
Limited paging capacity, PSM interval, or network overload are all possible factors that drive paging blocks. 
With reference to Fig.~\ref{paging}, once an MME sends
the S1 message, a timer called T3413 starts. If the timer expires before receiving
the target UE's paging response, the S1 message will be retransmitted, and the timer will be recounted according to the network operator's implementation.  
Flows cannot reach the end devices and have to be buffered until UEs wake up. \textit{The implication for \Name{}
is that inappropriate setting of energy-saving parameters might result in increased
paging blocking, thus increasing the delay before a device receives downstream traffic.}

\begin{figure}[htp]
    \centering
	\vspace*{-0.5cm}
    \includegraphics[scale=0.4]{figs/paginglatency.png}
    \vspace*{-0.3cm}
    \caption{Paging blocking}
    \vspace*{-0.2cm}
    \label{paging}
\end{figure}
\fi

\iffalse
\begin{table}[htbp]
%\caption{Timers associated with energy saving mechanisms}
\caption{Timers associated with energy saving mechanisms}
	\vspace*{-0.2cm}
\begin{center}
\scalebox{0.9}{
\begin{tabular}{|c|c|c|}
	\hline
	 \textbf{Timer}			&  \textbf{\textit{Value}}  & \textbf{\textit{Message to configure}}\\ \hline 
         Inactivity timer 			& 1 - 200 ms 																	& eNobed control\\ \hline
	 OnDurationTimer 		& 1 - 200 ms 						 											& rrcConnectionReconfig\\ \hline
	 cDRXcycle             		& \begin{tabular}[c]{@{}l@{}}LTE-M: 5.12 s, 10.24 s\\ NB: 0.256 - 9.216 s \end{tabular} 			& rrcConnectionReconfig\\ \hline
	 \textbf{T3324}                  	& 2 s - 410 hours  																& \begin{tabular}[c]{@{}l@{}}TAU ACCEPT \\ T3324 value IEI\end{tabular} \\ \hline
 	 \textbf{T3412}                  	& 2 s - 410 hours  																& \begin{tabular}[c]{@{}l@{}}TAU ACCEPT \\ T3412 value IEI\end{tabular} \\ \hline
	 \textbf{eDRXcycle}          	&\begin{tabular}[c]{@{}l@{}}LTE-M: 5.12 -  2621.44 s\\NB: 20.48 - 10485.76 s \end{tabular} 		& \begin{tabular}[c]{@{}l@{}}TAU ACCEPT \\ eDRX parameters IEI \end{tabular} \\ \hline
	 PTW          	       		&\begin{tabular}[c]{@{}l@{}}LTE-M: 1.28 -  20.48 s\\NB: 2.56 - 40.96 s \end{tabular} 				& \begin{tabular}[c]{@{}l@{}}TAU ACCEPT \\ eDRX parameters IEI \end{tabular} \\ \hline
\end{tabular}
}
\vspace*{-0.3cm}
\label{procedures}
\end{center}
\end{table}
\fi


\iffalse
\paragraph{Parameters Associated with Energy Saving Mechanism}
\label{sec:para_set}
\Name{} relies on the ability to adapt the parameters associated with
the energy-saving mechanisms in 3GPP IoT protocols. As such, we
need to consider which parameters might be useful for our purposes
and whether they can be changed without requiring changes to the relevant
3GPP protocols.
%Before designing the algorithms for selecting the optimal parameter settings, we need to understand how UEs and MMEs make agreements on the settings beneath the original 3GPP framework and whether it is possible to reconfigure the settings on the network side.
Table~\ref{procedures} lists the details of various RRC energy-saving mechanisms.
For \Name{} we are currently only making use of three of those,
namely T3324, T3412, and eDRXcycle. (We leave for future work exploring
the use of the PTW timer.)

In general, after the first RRC setup initiates the values, UEs and MMEs
negotiate RRC parameter settings via NAS layer messages with 
corresponding Information Element Identifiers (IEI) during RRC Connection
Reconfiguration and TAU procedures.
In legacy LTE, c-DRX parameters reconfiguration occurs during the RRC
Connection Reconfiguration. However, in 3GPP Release 15, Early Data Transmission
(EDT) introduces two EPS optimizations for IoT applications,
%to further reduce
%signaling costs by allowing one uplink data transmission optionally followed by
%one downlink data transmission during random access procedure, thus avoiding
%the very energy demanding connected state.
%The optimizations rule out the RRC Connection Reconfiguration for our purposes.
which rule out the RRC Connection Reconfiguration 
and apply event-triggered TAU for low battery or other application needs.
\textit{We consider in Section~\ref{sec:workflow} how this TAU can be utilized in the \Name{} workflow.}
\fi

\iffalse
\textit{Thus, for \Name{}, we only consider adapting
parameters associated with Tracking Area Update (TAU) interactions.}
As shown in Table~\ref{procedures}, TAU interactions are associated with
four timers. For \Name{} we are currently only making use of three of those,
namely T3324, T3412 and eDRXcycle. (We leave for future work exploring
use of the PTW timer.)
\fi




\iffalse
\textbf{RRC Connection Reconfiguration.}
In Legacy LTE procedures, c-DRX parameters reconfiguration happens during the RRC Connection Reconfiguration. However, in release 15, Early Data Transmission (EDT) introduces two EPS optimizations for IoT applications to further reduce signaling costs by allowing one uplink data transmission optionally followed by one downlink data transmission during random access procedure, which rules out the RRC Connection Reconfiguration. For NB-IoT, Control Plane EPS optimization is a mandatory mechanism to encapsulate and transmit data directly in NAS messages. Another option -User Plane EPS optimization- replaces the RRC Connection Setup with the RRC resume. Though LTE-M  follows Legacy LTE procedures, it has these optimizations as alternatives.

\iffalse
\begin{figure}[htp]
    	\centering
   	\includegraphics[width=9cm]{figs/SR.png}
	\caption[]{Service request with RRC reconfiguration is removed or replaced by EDT for NB-IoT in release 15}
\label{TAU}
\end{figure}
\fi
What's more, compare to other RRC parameters, c-DRX parameters modifying has lower power-saving efficiency considering the time interval and frequency of RRC connected state. Since there is less mutual communication between the end-devices and the application servers, reducing the RRC inactivity timers in eNodeB to shorten UEs' connected state time is more straightforward.
\fi

\iffalse
The basic TAU interaction involves a UE sending a Tracking Area Update Request,
followed by the MME responding with a Tracking Area Update Response.
\textit{The implication for \Name{} is that changes to RRC parameters can be
accomplished by setting those in the TAU Response message sent from the MME.}
There are a number of possible ``triggers'' (or options) that result in the UE sending
a TAU Request. We consider two such options for use in \Name{}.
First, TAU ``Periodic Updating'' happens when the periodic tracking area
updating timer T3412 expires indicating the end of the idle state
(see Fig.~\ref{fig-rrc}).
% 3GPP TS 23.301).
Second is TAU ``TA updating'', which can be sent ``on-demand'' by a UE
based on applications specific events, such as a low battery value etc.
\textit{We consider in Section~\ref{sec:workflow} how these
different TAU Request options can be utilized in the \Name{} workflow.}
\fi

% double blind...
%\paragraph{Traffic Patterns and Energy Use}
\subsection{Targeted Use Cases and Traffic Patterns}
Earlier work performed
measurements with IoT devices over commercial networks in Norway to assess how
traffic load and RCC parameter settings affect battery life expectancy~\cite{michelinakis2020dissecting}.
As expected, message frequency is the dominant factor determining lifetime,
followed by the setting of DRX, signal quality, and finally
packet size.
These findings further
%As discussed earlier, \Name{} does not use C-DRX. Our aim with these
%mo illustrate the impact of RRC parameter settings in general,
%and to 
\textit{motivate \Name{}'s approach to adapting energy saving parameters
based on traffic behavior}.

\begin{table}[htb!]
\renewcommand{\arraystretch}{1.3}
%	\scriptsize
\caption{Typical Traffic Patterns in Use Cases of Interest.}
\vspace*{-0.2cm}
\begin{center}
\scalebox{0.8}{
\begin{tabular}{|c|c|c|c|c|} \hline
       \textbf{Usecases}                          				& \textbf{\textit{MT Message Frequency}}           	& \textbf{\textit{Delay Tolerance}} 	                  \\ \hline 
        Lighting control                                 	                          & irregular    		                                          & 15 seconds      			                		 \\ \hline
       Smart Irrigation                                          		        & irregular	, infrequent                                         & 1 minute        		                      		 \\ \hline
       Smart appliances                  					& irregular, infrequent   		                          & 3 seconds       		                      		 \\ \hline
       Bike Sharing 								& irregular								 & 10 seconds 							 \\ \hline
      % Wearables                                        			& Every 10 sec. 	& 3-5 seconds     			& MO                \\ \hline
       %Wildlife tracking                              				& Every 30 min. 	& a few hours     			& MO                \\ \hline
       %\makecell{Vehicle localization \\ monitoring} 		& Every 30 sec. 	& 10 sec.         				& MO                \\ \hline
 \end{tabular}
 }
 \label{tab:traffic_patterns}
 \end{center}
\end{table}


Table~\ref{tab:traffic_patterns} presents various IoT actuator applications that predominantly wait for irregular mobile terminated (MT) messages, along with their associated delay tolerance as referenced in ~\cite{mocnej2018network}. It is worth noting that these applications typically perform periodic updates of their state or send heartbeat signals via MO messages. However, our primary focus in this context is on optimizing energy consumption for MT messages. Delivering timely and accurate commands to IoT devices through MT messages is vital, but may come at the cost of increased energy consumption.



%UEs can launch requests to reconfigure I-DRX and PSM parameters in the TAU request and reset the parameters after getting replies from MMEs. There are mainly two kinds of IEs encapsulated in the TAU request: mandatory IEs and optional IEs. The EPS update type is a mandatory IE to fill in the TAU request, which has "TA updating," "periodic updating," and other TAU options. "Periodic updating" shall be sent when the periodic tracking area updating timer T3412 expires (see 3GPP TS 23.301). "TA updating" is for specific events like parameter modifications or other requests when necessary (for reasons like low battery or pre-setting triggers). The selected EPS update type will not limit optional IEs like DRX parameter (in legacy LTE), T3324 value, T3412 value, and extended DRX parameters (in NB-IoT and LTE-M when eDRX is allowed), but "periodic updating" usually does not include those optional parameter settings' IEs. MMEs will include values of IEs from the TAU request the network can approve in the TAU accept.

\iffalse
\begin{figure}[htp]
    	\centering
   	\includegraphics[width=9cm]{figs/TAU.png}
	\caption[]{TAU procedure}
\label{TAU}
\end{figure}
\fi



\iffalse
Cellular IoT traffic can comprise either Mobile Originated (MO) or Mobile Terminated (MT) messages or in some cases a combination of both.
%In this paper, we only consider the first traffic direction in one communication. 
%\textcolor{blue}{In our paper, we only consider the first trafiic in the communication.}
%MO messages are sent by the UE once they are ready.
%In other words, a UE in a power saving state wakes up and sends MO messages as soon as there are ready for sending.
%In contrast, UEs will have to receive paging messages for fetching MT during the RRC
%connected state or DRX OnDuration interval.
%A UE in a power saving mode receives MT messages only when it wakes up and listens to paging messages.
Both types of traffic can be periodic or event-triggered.
Table~\ref{tab:traffic_patterns} showcases typical traffic patterns, the 
most common traffic direction and the delay tolerance of a selection of IoT
applications across a variety of sectors.
%Note that most of the MT usecases exhibit irregular transmissions, while
%having a delay tolerance of less than a minute. 
\fi



\iffalse
Mobile Originated (MO) and Mobile Terminated (MT) messages are two reverse
traffic in the LTE network. MO refers to messages that get sent once ready by
UEs.
In contrast, UEs have to receive paging messages for fetching MT during the RRC
connected state or DRX OnDuration interval.
Each of them can be classified into finer-grained types like periodic and
event-triggered traffic.

\begin{table}[htp]
\centering
\scriptsize
\caption[]{Traffic patterns and use cases }
\begin{tabular}{ c c c }
	\hline
     	\hline
	\textbf{Traffic pattern} & \textbf{Use cases} & \textbf{End device types} \\
	\hline
	Periodic MO &          & Sensor\\
	Event-triggered MO &          & Sensor\\
	Periodic MT &          & Actuator\\
	Event-triggered  MT &          & Actuator\\
	Mixed &          & Sensor-Actuator \\
	\hline
        \hline
\label{tab-patterns}
\end{tabular}
\end{table}


Table~\ref{tab:traffic_patterns} shows typical traffic patterns, the 
most common traffic direction (mobile originated (MO) and/or mobile
terminated (MT)), and the delay tolerance of a selection of IoT
applications across a variety of sectors~\cite{mocnej2018network}.
Most sensor-based IoT use cases have regular MO traffic patterns.
Their MT traffic is usually not real-time (e.g., firmware or software update), which can wait for wakeup cycles.
The current DRX and PSM mechanisms are well-matched with the needs and expectations of such use cases.
Actor-based IoT use cases often have a mix of MT and MO traffic.
Commands to execute certain actions are conveyed via MT messages.
Hence, the ability to timely relay these messages to IoT devices becomes a critical requirement for such use cases.
This, in turn, may come at the cost of higher energy consumption.
While the tradeoff between timeliness and energy efficiency is easy to resolve for main-powered IoT devices, 
many use cases are based on battery-powered sensor-actuator devices.
\Name{} {\em aims to provide a tunable tradeoff between energy efficiency and communication timeliness}.
\fi

\iffalse
Different applications lead to polarized demand for data transmission. Most sensor-based IoT use cases have regular and permanent traffic patterns for MO service and usually no strict time limit to receive the downlink MT traffic for firmware or software updates. Thus, the current DRX and PSM mechanisms are of great benefit. Actor-based IoT devices, usually mains powered or at least heavy-duty, have more energy resources than sensor-based devices, care more about the instantaneity than the transmission's energy consumption. However, the boundary is blurred; some scenarios need a compromise solution badly. Battery-based sensor-actuator devices are seeking a movable balance between the interactive struggles on both sides according to their service demands. The difficulty lies in that S1 paging for MT has a chance to wait in buffers for a usable Paging Occasion and a successful Random Access end, which brings about the dilemma of the DRX and PSM parameter settings.
\fi

\iffalse
\subsection{Illustrative energy measurements}

\begin{figure}[htp]
\centering
   \includegraphics[width=6cm, height=6cm]{figuresAndCode/energy_consumption_toy_example/energy_consumption_toy_example.png}
   \caption{Life expectancy of an IoT device
   as a function of traffic load and RCC parameters settings, based on the NB-IoT SARA-N211
   module~\cite{michelinakis2020dissecting}.}
   % The network used is Telia in Norway.
\label{fig:energy_consumption_toy_example}
\end{figure}


In Fig.~\ref{fig:energy_consumption_toy_example}, we present energy
measurements illustrating the impact of different RRC parameter
settings on battery life. The measurements are from 
an IoT device equipped with an NB-IoT SARA-N211 module, the same
module we use in our simulations presented in the sequel.
The results are from real world measurements of the
module~\cite{michelinakis2020dissecting}.
According to 3GPPP targets~\cite{3gpp45820} IoT devices should achieve ``up
to ten years battery life with battery capacity of 5 Wh (Watt-hours), even in
locations with adverse coverage conditions'' in an NB-IoT scenario, so we assume
a 5 Wh (18000 Joule) battery.

The bar plots are arranged in grid where the rows indicate the use of RAI flag
(i.e., whether the device performs C-DRX or transits to idle immediately after
transmission)
and the columns the signal conditions in the device location.
As expected message frequency is the dominant factor determining lifetime.
For typical use cases, where very small messages are exchanged, the majority of
energy of the connected state is consumed while performing C-DRX.
Because of that, packet size is important mostly when the RAI flag is also used,
because otherwise C-DRX, dominates energy consumption.
Thus, the rest of the factors ordered by importance are: use of RAI flag, signal
quality and finally packet size.

As discussed earlier, \Name{} does not use C-DRX. Our aim with these
measurements is to illustrate the impact of RRC parameter settings in general,
and to \textit{motivate \Name{}'s approach to adapting RRC parameters
based on traffic behavior}.
\fi